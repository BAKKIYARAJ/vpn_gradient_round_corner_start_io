package com.vpnmastersm.singaporevpnmaster;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;
import com.vpnmastersm.singaporevpnmaster.adapter.ServerCountryAdapter;
import com.vpnmastersm.singaporevpnmaster.adapter.ServerLocation;
import com.vpnmastersm.singaporevpnmaster.autoconfig.AutoConfigVPN;
import com.vpnmastersm.singaporevpnmaster.vpnhelper.CountryPrefs;
import com.vpnmastersm.singaporevpnmaster.vpnhelper.VPNServerHelper;

import java.util.Arrays;
import java.util.List;

import de.blinkt.openvpn.core.ConnectionStatus;
import de.blinkt.openvpn.core.VpnStatus;


public class ActivityVpn extends AppCompatActivity implements VpnStatus.StateListener, ServerCountryAdapter.LocationClickListener {

    private AutoConfigVPN mConfig;
    private TextView txtConnectionStatus, taplocation;
    AppCompatImageView ivConnected, ivDisConnected;
    ProgressBar progress_bar;
    private ServerCountryAdapter scadapter;
     public static final String MyPREFERENCES = "MyPrefs";
    ConstraintLayout connectButton, Constlocation, iVmenu;
    private boolean isConnected = false;
    private String connectedCountry;
    //public SquareProgressBar squareProgressBar;
    int i = 0;
    boolean CONNECTED;
    //  private LinearLayout banner_container;

    private Handler handler = new Handler();

    private Runnable runnable = () -> {
        if (!isConnected) {
            //vpnServerHelper.disconnectFromVpn();
            SharedPreferences sharedpreferences = getSharedPreferences("VPN", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("ServerName", "Server Disconnected");
            editor.apply();
        }
    };

    // private InterstitialAd interstitialAd;
    private AlertDialog RateAppDialog;
    // private AdView adView;
    //  private InterstitialAd interstitialAd;

    private VPNServerHelper vpnServerHelper;
    private CountryPrefs countryPrefs;
    Dialog dialog;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vpn);
        initView();
        List<String> testDeviceIds = Arrays.asList("33BE2250B43518CCDA7DE426D04EE231");
        RequestConfiguration configuration =
                new RequestConfiguration.Builder().setTestDeviceIds(testDeviceIds).build();
        MobileAds.setRequestConfiguration(configuration);


        mConfig = new AutoConfigVPN(this);
        vpnServerHelper = new VPNServerHelper(this, mConfig);
        countryPrefs = new CountryPrefs(this);
        dialog = new Dialog(this, R.style.BottomDialogs);
    }


    private void initView() {


        txtConnectionStatus = findViewById(R.id.txtConnectionStatus);
        taplocation = findViewById(R.id.taplocation);

        connectButton = findViewById(R.id.ConstConnect);

        Constlocation = findViewById(R.id.Constlocation);
        iVmenu = findViewById(R.id.iVmenu);
        ivDisConnected = findViewById(R.id.ivDisConnected);
        progress_bar = findViewById(R.id.progress_bar);
        ivConnected = findViewById(R.id.ivConnected);
      /*  squareProgressBar = findViewById(R.id.progress);
        squareProgressBar.setColor("#FF6028DF");
        squareProgressBar.setWidth(12);
        squareProgressBar.setRoundedCorners(true, 50);
        squareProgressBar.setProgress(100);*/

        connectButton.setOnClickListener(view1 -> {
            boolean isActive = VpnStatus.isVPNActive();
            if (!isActive) {
                String name = VPN_Utils.getRandomFastServer();
                countryPrefs.setSelectedLocation(name.split("\\.")[0]);
                VPNConnect(name);
                String server_name = name.split("\\.")[0];
                SharedPreferences sharedpreferences = this.getSharedPreferences("VPN", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("ServerName", server_name);
                editor.apply();

                Log.e("Gokul error ", "Server name " + name);
                //  showInterstitial();
//                Intent intent = new Intent(this.this, NativeAdActivity.class);
//                startActivity(intent);

            } else {
                CONNECTED = true;
                vpnServerHelper.disconnectFromVpn();
                SharedPreferences sharedpreferences = this.getSharedPreferences("VPN", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("ServerName", "Server Disconnected");
                editor.apply();
                // showInterstitial();

////gokull
//                Intent intent = new Intent(this.this, NativeAdActivity.class);
//                startActivity(intent);


            }
            //StartAppAd.showAd(this);

        });

        Constlocation.setOnClickListener(v -> showLocationSelectDialog());
        iVmenu.setOnClickListener(v -> {
            Intent intent = new Intent(ActivityVpn.this, NavActivity.class);
            startActivity(intent);
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        release();
    }

    @Override
    public void onResume() {
        super.onResume();
        VpnStatus.addStateListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        removeCallbacks();
        release();
    }

    private void removeCallbacks() {
        try {
            handler.removeCallbacks(runnable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void release() {
        VpnStatus.removeStateListener(this);
    }


    private void showLocationSelectDialog() {

        dialog.setContentView(R.layout.bottom_sheet);
        scadapter = new ServerCountryAdapter(this, VPN_Utils.getLocatons(),
                this, dialog, connectedCountry);

        RecyclerView recyclerView = dialog.findViewById(R.id.rv_locations);
        ImageView close = dialog.findViewById(R.id.iv_back);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(scadapter);

        close.setOnClickListener(v -> dialog.dismiss());
        if (dialog.getWindow() != null) {
            dialog.getWindow().addFlags(Window.FEATURE_NO_TITLE);
        }

        if (dialog.getWindow() != null) {
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        }
        dialog.show();
    }


    @Override
    public void updateState(String state, String logmessage, int localizedResId, final ConnectionStatus level, Intent intent) {

        this.runOnUiThread(() -> {
            String stateMessage = VpnStatus.getLastCleanLogMessage(ActivityVpn.this);
            updateUI(stateMessage, level);
            Log.e("Vidhu", "Level UpdateState Called------------" + level);
            System.out.println("RAJ_CHECK->" + "updateState");
        });
    }

    private void RateAppDialog() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Rate Us");
        builder.setMessage("Please take a moment to Rate our Application");
        builder.setPositiveButton("Yes", (dialog, which) -> {
            dialog.dismiss();

            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.vpnmastersm.uaevpn")));

        });

        builder.setNegativeButton("Later", (dialog, which) -> dialog.dismiss());


        RateAppDialog = builder.create();
        RateAppDialog.show();

    }

    @Override
    public void setConnectedVPN(String uuid) {
    }


    @SuppressLint("ResourceAsColor")
    private void updateUI(String stateMessage, ConnectionStatus level) {

        String selectedLocation = countryPrefs.getSelectedLocation();

        if (level == ConnectionStatus.LEVEL_CONNECTED) {
            connectButton.setEnabled(true);
            taplocation.setText(String.format("%s To : %s", getString(R.string.connected), selectedLocation));
            SharedPreferences sharedpreferences = this.getSharedPreferences("VPN", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("ServerName", selectedLocation);
            editor.apply();
            Constlocation.setClickable(false);
            isConnected = true;
            connectedCountry = stateMessage;
           // StartAppAd.showAd(this);
            UIChange(true);
            System.out.println("RAJ_CHECK->" + "LEVEL_CONNECTED");

        } else if (level == ConnectionStatus.LEVEL_NOTCONNECTED) {

            connectButton.setEnabled(true);
            SharedPreferences sharedpreferences = this.getSharedPreferences("VPN", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("ServerName", "Server Disconnected");
            editor.apply();
            taplocation.setText(getString(R.string.tap_region));
            Constlocation.setClickable(true);
            connectedCountry = null;
            UIChange(false);
            System.out.println("RAJ_CHECK->" + "LEVEL_NOTCONNECTED");

        } else {
            UIChange(false);
            connectButton.setEnabled(true);
            connectedCountry = stateMessage;
            txtConnectionStatus.setText("CONNECTING");
            ivConnected.setVisibility(View.GONE);
            ivDisConnected.setVisibility(View.GONE);
            progress_bar.setVisibility(View.VISIBLE);
            System.out.println("RAJ_CHECK->" + "ELSE");

        }
    }


    @Override
    public void onClickItemListener(int position, ServerLocation serverLocation) {

        connecttovpn(position, serverLocation.getName(), VPN_Utils.getRandomServer(serverLocation.getVpnFileName()));
        SharedPreferences sharedpreferences = this.getSharedPreferences("VPN", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("ServerName", serverLocation.getName());
        editor.apply();
        // showInterstitial();
    }


    private void startTimer() {
        removeCallbacks();
        handler.postDelayed(runnable, 10000);
    }


    private void connecttovpn(final int position, final String location, final String locationFileName) {
        countryPrefs.setSelectedLocation(location);
        VPNConnect(locationFileName);
    }


    private void VPNConnect(final String locationFileName) {
        startTimer();
        String server_name = locationFileName.split("\\.")[0];
        SharedPreferences sharedpreferences = getSharedPreferences("VPN", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("ServerName", server_name);
        editor.apply();
        boolean value = vpnServerHelper.connectOrDisconnect(locationFileName);
        if (value) {
            Log.e("Vidhu", "Location servername- TRUE------------" + server_name);
            System.out.println("RAJ_CHECK->" + "VPNConnect if");

            // showInterstitial();
        } else {
            Log.e("Vidhu", "Location servername- TRUE------------" + server_name);
            System.out.println("RAJ_CHECK->" + "VPNConnect if");

        }
    }


    private void UIChange(boolean status) {
        if (status) {
            txtConnectionStatus.setText("CONNECTED");
            ivConnected.setVisibility(View.GONE);
            progress_bar.setVisibility(View.GONE);
            ivDisConnected.setVisibility(View.VISIBLE);
        } else {
            txtConnectionStatus.setText("CONNECT");
            ivConnected.setVisibility(View.VISIBLE);
            ivDisConnected.setVisibility(View.GONE);
            progress_bar.setVisibility(View.GONE);

        }
    }
}